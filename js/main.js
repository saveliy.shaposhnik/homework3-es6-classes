class Employee{
    constructor({ name = "unknown",age = "unknown",salary = "unknown" }){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    set name(value){
        this._name = value;
    }
    get name(){
        return this._name;
    }
    set age(value){
        this._age = value < 0 ? 0 : value;
    }
    get age(){
        return this._age;
    }
    set salary(value){
        this._salary = value;
    }
    get salary(){
        return this._salary;
    }
}
const user1 = new Employee({name:"Sasha",age:33,salary:"2000$"})
console.log(user1);
const user2 = new Employee({name:"Lesha",age:24,salary:"1000$"})
console.log(user2);

class Programmer extends Employee{
    constructor({ name = "unknown",age = "unknown",salary = "unknown",lang = "unknown" }){
        super({ name,age,salary })
        this.lang = lang;
    }
    set salary(value){
        this._salary  = `${value * 3}$`;
    }
    get salary(){
        return this._salary;
    }
}
